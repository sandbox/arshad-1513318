<?php

/**
 * @file
 * Plugin for node_body_paging
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Node body (Paginated)'),
  'icon' => 'icon_node.png',
  'description' => t('The body of the referenced node paginated using the Paging module.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'category' => t('Node'),
);

/**
 * Render the custom content type.
 */
function paging_panels_node_body_paging_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return;
  }

  // Get a shortcut to the node.
  $node = $context->data;

  // Load information about the node type.
  $type = node_get_types('type', $node->type);

  // Do not render if the body is disabled for this node type.
  if (!$type->has_body) {
    return;
  }
  
  //build the node content
  node_build_content($node);

  // Build the content type block.
  $block = new stdClass();
  $block->module  = 'node_body';
  $block->title   = $type->body_label;
  $block->content = drupal_render($node->content['body']); //render the body field
  $block->content .= drupal_render($node->content['paging']); //render the pager
  $block->delta   = $node->nid;

  return $block;
}

/**
 * Returns an edit form for custom type settings.
 */
function paging_panels_node_body_paging_content_type_edit_form(&$form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
}

/**
 * Returns the administrative title for a type.
 */
function paging_panels_node_body_paging_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" body', array('@s' => $context->identifier));
}

